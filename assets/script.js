// Hooman
 let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}


introduce({
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
});

function introduce({name, age, classes}){

	console.log(`Hi! I'm ${name}. I am ${age} years old.`);
	console.log(`I study the following courses ${classes}`);
}


const getCube = (num) => Math.pow(num, 3);
let cube = getCube(3);

let numArr = [15,16,32,21,21,2];

numArr.forEach(num => console.log(num));


const numSquared = numArr.map(num => Math.pow(num, 2));
console.log(cube);
console.log(numSquared);


// Doggos
class Dog{
	constructor(name, breed, dogAge, humanYears){
		this.name = name;
		this.breed = breed;
		this.dogAge = 7 * humanYears;
		this.humanYears = humanYears;
	}
};
let dog1 = new Dog('Benny Bentong', 'Poodle', 7, 1);
let dog2 = new Dog('Koko Toasty', 'Borgi', 14, 2);
let dog3 = new Dog('Nikki Fatso', 'Pomeranian', 84, 12);


console.log(dog1);
console.log(dog2);
console.log(dog3);